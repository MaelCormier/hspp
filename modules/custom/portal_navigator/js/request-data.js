/**
 * Request Data
 */
;(function (window, document, $, undefined) {

  "use strict";

  $.extend(Drupal.theme, {
    /**
    * Generic function to make an AJAX call
    * to a localized copy of JSON or XML
    * 
    * @param {string} dataURL
    * @param {string} type
    * @return {function}
    * 
    * 	Drupal.theme('fetchData', 'dataURL', 'type');
    */
    fetchData: function(dataURL,type) {     
      return $.ajax({
          contentType: 'application/' + type + '; charset=UTF-8',
          url: dataURL + '?_format=' + type,
          method: 'GET',
          isLocal: true,
          crossDomain: false,
      });
    }
  });
}(window, document, jQuery));