(function ($, Drupal, drupalSettings) {

    "use strict";

    Drupal.behaviors.portalNavigator = {
        attach: function (context, settings) {
            $(window, context).once('portalNavigator').each(function () {
              // add code here to avoid being triggered every AJAX request
              /**
               * Expand All items in Accordion
               */
              $(document).on('click', '.expand-all', function(event) {
                event.preventDefault();
                Drupal.theme('expandCollapse', 'expand', this);
              });

              /**
               * Collapse All items in Accordion
               */
              $('.collapse-all').on('click', function(event) {
                event.preventDefault();
                Drupal.theme('expandCollapse', 'collapse', this);
              });

              // Modal
              $('#exampleModalCenter').on('show.bs.modal', function (event) {
                const button = $(event.relatedTarget); // Button that triggered the modal
                let title = button.data('title'); // Extract info from data-* attributes
                let embed = (button.data('embed'));
                let options = button.data('options');

                const modal = $(this);
                modal.find('.modal-title').text(title);
                if (embed) { modal.find('#framecontainer').html('<iframe src="' + embed + '" width="100%" height="100%"></iframe>'); }
                (options === true) ? modal.find('#report-filters').show(): modal.find('#report-filters').hide();
              });

              /**
               *
               */
              $(document).on('click', '.retrieve-content', function (event) {
                event.preventDefault();
                const url = 'node/' + $(this).data('id');
                const request = Drupal.theme('fetchData', url, 'json');
                const card_body = '#definition-summary .card-body';
                const open_report = '#definition-summary .open-report';
                $('#report-filters').html('');

                $.when(request).then(function (data) {
                  $(card_body + ' .new').show();
                  $(card_body + ' .default').hide();
                  $(card_body + ' .new').css('opacity', 0);
                  $(card_body + ' .new .title').html(data.title[0].value);
                  if (data.body[0]) {
                    var desc = (data.body[0]) ? data.body[0].value : '';
                    var sub = (data.field_technical_name[0]) ? '<h5>' + data.field_technical_name[0].value + '</h5>' : '';
                    var embed = (data.field_embed_code[0]) ? data.field_embed_code[0].value : '';

                    $('#definition-summary .description').html(sub + desc);
                    $(open_report).show();
                    $(open_report).data('title', data.title[0].value);
                    $(open_report).data('embed', embed);

                  }
                  else {
                    $('#definition-summary .description').html('Description not available.');
                    $(open_report).hide();
                  }
                  if (typeof data.field_references === 'object' && data.field_references[0] != null) {
                    let options_list = '<div class="btn-group"><button class="btn btn-secondary btn-sm dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Select An Option</button><div id="video-embed-options" class="dropdown-menu"><h6 class="dropdown-header">Report Options</h6>' +
                      '<a href="' + embed + '" class="dropdown-item">' + data.title[0].value + '</a>' +
                      Drupal.theme('buildListOfLinks', data.field_references) +
                      '</div></div>';
                    let related_reports = '<li><a href="#">Report 4</a></li><li><a href="#">Report 5</a></li><li><a href="#">Report 6</a></li>';
                    $(open_report).data('options', true);
                    // let more_filters = '<div class="btn-group"><button class="btn btn-secondary btn-sm dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">More Options</button><div id="video-embed-options" class="dropdown-menu" x-placement="bottom-start" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(0px, 30px, 0px);"><h6 class="dropdown-header">More Options</h6><a href="https://www.youtube.com/embed/zyYgDtY2AMY" class="dropdown-item">Clinical Efficacy and Effectiveness</a><a href="https://www.youtube.com/embed/zyYgDtY2AMY?start=0" class="dropdown-item">From Beginning</a><a href="https://www.youtube.com/embed/zyYgDtY2AMY?start=10" class="dropdown-item">After10 Seconds</a><a href="https://www.youtube.com/embed/zyYgDtY2AMY?start=30" class="dropdown-item">After 30 Seconds</a><a href="https://www.youtube.com/embed/TVowQ4LgwLk" class="dropdown-item">Next Trailer</a></div></div>';
                    $('#report-filters').html(options_list);
                    $('#related-reports').html(related_reports);
                  }
                  else {
                    $(open_report).data('options', false);
                  }
                  $(card_body + ' .new').animate({opacity: 1}, 2500, function () {});
                });
              });

              //
              $(document).on('click', '#tiles .list-group-horizontal .list-group-item-action h3', function() {
                $('#definition-summary .card-body .new').hide();
                $('#definition-summary .card-body .default').show();
              });

              //
              $('#list-tab .list-group-item-action h3').on('click', function(event) {
                event.preventDefault();
                $('#tiles .full-title .col').html('<p class="' + $(this).data('section') + '">' + $(this).data('definition') + '</p>');
              });

              // Embed Dropdown
              $(document).on('click', '#video-embed-options .dropdown-item', function(event) {
                  event.preventDefault();
                  const embed = $(this).attr('href');
                  const url = new URL(embed);
                  $('#framecontainer iframe').attr('src', embed);
                  localStorage.setItem('dropDownSelection', url.searchParams.get("start"));
                  $('#related-reports li a').each(function (index, value) {
                    const href = $(this).attr('href');
                    $(this).attr('href', (href + '?start=' + url.searchParams.get("start")));
                  });
              });
            });
        },
    };
})(jQuery, Drupal, drupalSettings);
