<?php

namespace Drupal\portal_navigator\Plugin\Block;

use Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException;
use Drupal\Component\Plugin\Exception\PluginNotFoundException;
use Drupal\Core\Block\BlockBase;
use Drupal\taxonomy\Entity\Term;

/**
 * Provides a 'PortalNavigatorBlock' block.
 *
 * @Block(
 *  id = "portal_navigator_block",
 *  admin_label = @Translation("Portal Navigator"),
 * )
 */
class PortalNavigatorBlock extends BlockBase {

   /**
   * Implements \Drupal\Core\Block\BlockBase::blockBuild()
   *
   */
  public function build() {
    return array(
      '#theme' => 'portal_navigator',
      '#description' => 'lorem ipsum dolor sit amet',
      '#report_categories' => $this->getTermData(),
      '#health_outcomes_and_experiences' => $this->getNavSectionData(1),
      '#understand_health_needs' => $this->getNavSectionData(2),
      '#meet_health_needs' => $this->getNavSectionData(3),
      '#support_service_providers' => $this->getNavSectionData(4),
      '#enable_technology_and_infrastructure' => $this->getNavSectionData(5),
    );
  }

  /**
   *
   */
  private function getTermData() {
    $terms =\Drupal::entityTypeManager()->getStorage('taxonomy_term')->loadTree('category');
    $image = '';
    $term_data = [];

    foreach ($terms as $term) {
        if ($term->depth == 0) {
            $term_obj = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->load($term->tid);
            if(isset($term_obj->get('field_image')->entity)){
              $image = file_create_url($term_obj->get('field_image')->entity->getFileUri());
            }
            $term_data[] = array(
              'id' => $term->tid,
              'name' => $term->name,
              'icon' => $term_obj->get('field_icon')->value,
              'long_title' => $term_obj->get('field_long_title')->value,
              'image' => $image,
              'description' => $term_obj->get('description')->value
            );
        }
    }
    return $term_data;
  }

  /**
   * @param $id
   * @return array
   * @throws InvalidPluginDefinitionException
   * @throws PluginNotFoundException
   */
  private function getNavSectionData($id) {
    $terms = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->loadChildren($id);
    $report_data = [];
    $section = [];
    $child = [];

    foreach ($terms as $term) {
      $child[] = $term->get('tid')->value;
    }

    $reports = \Drupal::entityTypeManager()->getListBuilder('node')->getStorage()
      ->loadByProperties([
        'type' => 'report',
        'status' => 1,
        'field_report_category' => $child
    ]);

    foreach ($reports as $report) {
      $this_term = Term::load($report->get('field_report_category')->target_id);

      $report_data[] = array(
          'nid' => $report->get('nid')->value,
          'title' => $report->get('title')->value,
          'category' => $report->get('field_report_category')->target_id,
          'category_name' => $this_term->getName()
        );
    }

    foreach ($child as $category) {
      foreach ($report_data as $item) {
        if($category == $item['category']) {
          $section[$category][] = $item;
        }
      }
     }

     return $section;

  }

}
