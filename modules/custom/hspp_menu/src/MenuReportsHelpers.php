<?php

namespace Drupal\hspp_menu;

use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Menu\MenuLinkManagerInterface;
use Drupal\Core\Menu\MenuLinkTreeInterface;
use Drupal\Core\Menu\MenuTreeStorageInterface;
use Drupal\menu_link_content\MenuLinkContentStorageInterface;

class MenuReportsHelpers {

  /**
   * The menu link tree service.
   *
   * @var \Drupal\Core\Menu\MenuLinkTreeInterface
   */
  protected $menuLinkTree;

  /**
   * @var MenuTreeStorageInterface
   */
  protected $menuTreeStorage;

  /**
   * The menu link plugin manager.
   *
   * @var \Drupal\Core\Menu\MenuLinkManagerInterface
   */
  protected $menuLinkManager;

  /**
   * The menu_link_content storage handler.
   *
   * @var \Drupal\menu_link_content\MenuLinkContentStorageInterface
   */
  protected $menuLinkContentStorage;

  /**
   * Constructs the Menu Helpers service.
   *
   * @param \Drupal\Core\Menu\MenuLinkTreeInterface $menu_link_tree
   *   The menu link tree service.
   * @param \Drupal\Core\Menu\MenuTreeStorageInterface $menu_tree_storage
   * @param \Drupal\Core\Menu\MenuLinkManagerInterface $menu_link_manager
   *   The menu link plugin manager.
   * @param \Drupal\menu_link_content\MenuLinkContentStorageInterface $menu_link_content_storage
   *   The menu link content storage handler.
   */
  public function __construct(MenuLinkTreeInterface $menu_link_tree, MenuTreeStorageInterface $menu_tree_storage, MenuLinkManagerInterface $menu_link_manager, MenuLinkContentStorageInterface $menu_link_content_storage) {
    $this->menuLinkTree = $menu_link_tree;
    $this->menuTreeStorage = $menu_tree_storage;
    $this->menuLinkManager = $menu_link_manager;
    $this->menuLinkContentStorage = $menu_link_content_storage;
  }

  /**
   * Manager to add children reports to the sidebar menu
   *
   * @param \Drupal\taxonomy\TermInterface $term
   *
   */
  public function manageMenuCategoryPage($term) {
    $uuid = $this->getParentUUID($term);
    if (!is_null($uuid)) {
      $reports = $this->getReferencedNodeReports($term);
      $this->addLink($reports,$uuid);
    }
  }

  private function getParentUUID($term) {
    $uuid = null;
    $term_id = $term->id();
    $menu_link_parent = $this->menuLinkManager->loadLinksByRoute('entity.taxonomy_term.canonical', array('taxonomy_term' => $term_id));
    if (!empty($menu_link_parent)) {
      $uuid = array_values($menu_link_parent)[0]->getPluginId();
    }
    // Get the sidebar menu tree
    $tree = $this->menuLinkTree->load('sidebar-navigation', new \Drupal\Core\Menu\MenuTreeParameters());
    foreach ($tree as $entry) {
      $tid = $entry->link->getRouteParameters()['taxonomy_term'];
      if ($tid == $term_id) {
        $uuid = $entry->getDerivativeId();
        break;
      }
      elseif (!empty($entry->subtree)) {
        foreach ($entry->subtree as $subentry) {
          $tid = $subentry->link->getRouteParameters()['taxonomy_term'];
          if ($tid == $term_id) {
            $uuid = $subentry->link->getDerivativeId();
            break 2;
          }
        }
      }
    }

    return $uuid;
  }

  private function emptyChildrenLinks($uuid_parent) {

  }

  private function getReferencedNodeReports ($term) {
    $reports = $term->field_children_reports->referencedEntities();
    return $reports;
  }

  private function addLink ($reports, $parent_uuid) {
    foreach ($reports as $report) {
      $menu_link = MenuLinkContent::create([
        'title' => $report->title(),
        'link' => ['uri' => 'node/'.$report->id()],
        'menu_name' => 'sidebar-menu',
        'parent' => $parent_uuid,
        'expanded' => TRUE,
        'weight' => 0,
      ]);
      $menu_link->save();
    }
  }

}
