<?php

namespace Drupal\hspp_panels\Plugin\Condition;

use Drupal\Core\Condition\ConditionPluginBase;
use Drupal\Core\Plugin\Context\ContextDefinition;

/**
 * Provides a 'Is panorama report type' condition to enable a condition based in module selected status.
 *
 * @Condition(
 *   id = "is_panorama_report_type",
 *   label = @Translation("Is panorama report type"),
 *    context = {
 *     "node" = @ContextDefinition("entity:node",
 *     label = @Translation("Node")
 *     ),
 *     "report_type" = @ContextDefinition("entity:taxonomy_term",
 *     label = @Translation("Report Type")
 *     )
 *   }
 * )
 *
 */
class IsPanoramaReportType extends ConditionPluginBase {

  /**
   * {@inheritdoc}
   */
  public function evaluate() {
    $pass = FALSE;

    /** @var Node $node */
    $node = $this->getContextValue('node');
    if (!$node instanceof \Drupal\node\Entity\Node) {
      return $pass;
    }

    /** @var \Drupal\taxonomy\Entity\Term $node */
    $term = $this->getContextValue('report_type');
    if (!$term instanceof \Drupal\taxonomy\Entity\Term) {
      return $pass;
    }

    if (!$node->hasField('field_report_type')) {
      return $pass;
    }

    if ($node->get('field_report_type')->getString() == $term->id()) {
      $pass = TRUE;
    }

    return $pass;
  }

  /**
   * {@inheritdoc}
   */
  public function summary() {
    return $this->t('This report is a panorama report.');
  }

}
