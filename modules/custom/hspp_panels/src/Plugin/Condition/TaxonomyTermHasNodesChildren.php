<?php

namespace Drupal\hspp_panels\Plugin\Condition;

use Drupal\Core\Condition\ConditionPluginBase;
use Drupal\Core\Plugin\Context\ContextDefinition;

/**
 * Provides a 'Taxonomy Term Has Nodes Children' condition to enable a
 * condition based in module selected status.
 *
 * @Condition(
 *   id = "taxonomy_term_has_nodes_children",
 *   label = @Translation("This Entity has Nodes Children (Nodes reference)"),
 *    context = {
 *     "taxonomy_term" = @ContextDefinition("entity:taxonomy_term",
 *     label = @Translation("{taxonomy_term} from route")
 *     )
 *   }
 * )
 *
 */
class TaxonomyTermHasNodesChildren extends ConditionPluginBase {

  /**
   * {@inheritdoc}
   */
  public function evaluate() {
    $pass = FALSE;

    /** @var \Drupal\taxonomy\Entity\Term $node */
    $term = $this->getContextValue('taxonomy_term');
    if (!$term instanceof \Drupal\taxonomy\Entity\Term) {
      return $pass;
    }

    if (!is_null($term->field_children_reports->target_id)) {
      $pass = TRUE;
    }

    return $pass;
  }

  /**
   * {@inheritdoc}
   */
  public function summary() {
    return $this->t('Condition to check of this Taxonomy Term has Reports as children referenced values.');
  }

}
