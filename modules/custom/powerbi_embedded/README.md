# Power BI Embedded

## Description

If you are developing an application which embeds reports you own, you are in the right section. In this scenario, you have to create Power BI account, and upload your reports to the created account. Then, you can call Power BI APIs to generate Embed Token for reports, dashboards or tiles you want to embed. For more information about embedding entities using Embed Tokens, please visit Embed Token Documentation. In this scenario, set TokenType to "Embed". once you have the embed token, use the following code to embed:

```javascript
// Get models. models contains enums that can be used.
var models = window['powerbi-client'].models;

var embedConfiguration = {
  type: 'report',
  id: '5dac7a4a-4452-46b3-99f6-a25915e0fe55',
  embedUrl: 'https://app.powerbi.com/reportEmbed',
  tokenType: models.TokenType.Embed,
  accessToken: 'h4...rf'
};

var $reportContainer = $('#reportContainer');
var report = powerbi.embed($reportContainer.get(0), embedConfiguration);
```

## Resources

[Register an Azure AD application to use with Power BI](https://docs.microsoft.com/en-us/power-bi/developer/register-app)
[PowerBI-JavaScript](https://github.com/microsoft/PowerBI-JavaScript)
[Embedding Basics](https://github.com/Microsoft/PowerBI-JavaScript/wiki/Embedding-Basics)
[App Owns Data](https://app.powerbi.com/embedsetup/appownsdata)
[Power BI REST APIs](https://docs.microsoft.com/en-us/rest/api/power-bi/)
[Power BI Embedded Azure Resource Manager REST API reference](https://docs.microsoft.com/en-us/rest/api/power-bi-embedded/)
[Embedded Analytics with Power BI](https://www.taygan.co/blog/2018/05/14/embedded-analytics-with-power-bi)

### Power BI Report Server

[Develop with the REST APIs for Power BI Report Server](https://docs.microsoft.com/en-us/power-bi/report-server/rest-api)
[Power BI Report Server REST API](https://app.swaggerhub.com/apis/microsoft-rs/PBIRS/2.0)
[Embedding Reports](https://docs.microsoft.com/en-us/power-bi/report-server/quickstart-embed)

## Reports Server

[Power BI Report Server](pwrbistg.hlth.gov.bc.ca/Reports)
[Ballmer](http://ballmer.idir.bcgov/Reports/powerbi/CrossSectorAnalysis/Sprint%203%20DB?rs:embed=true)
