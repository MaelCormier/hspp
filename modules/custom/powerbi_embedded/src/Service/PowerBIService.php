<?php

namespace Drupal\powerbi_embedded\Service;

/**
 * A simple exampe of a Drupal 8 service.
 * 
 * Usage:
 * 
 * $service = \Drupal::service('powerbi_embedded_service');
 * print_r($our_service->embedToken());
 * 
 */
class PowerBIService {

  /**
   * Returns an Embed Token.
   */
  public function embedToken() {
      return 'Embed Token';
  }

  /**
   * Returns an AAD Token
   */
  public function aadToken() {
      return 'AAD Token';
  }

}