<?php
/**
 * @file
 * Contains \Drupal\powerbi_embedded\Controller\PowerBIEmbeddedController.
 * 
 * 
 */
namespace Drupal\powerbi_embedded\Controller;

use Drupal\Core\Controller\ControllerBase;

class PowerBIEmbeddedController extends ControllerBase {

  /**
   * The Construct
   */
  public function __construct() { }

  /**
   * 
   */
  public function content()  {
    $build = array(
      '#theme' => 'powerbi_embedded',
      '#description' => 'lorem ipsum dolor sit amet',
      '#attached' => array(
        'library' => array(
          'powerbi_embedded/base',
        ),
      )
    );
    return $build;
  }

}