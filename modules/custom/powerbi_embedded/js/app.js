/**
 * 
 * version 0.1
 * 
 */
(function ($, Drupal, drupalSettings) {

    "use strict";

    var txtAccessToken = 'H4sIAAAAAAAEAB2WxcrGDJJG7-XfpiFuDb2Iu3t28eSNuwxz7_NN7x8oOHXqof7nHzt7hzkr__n3P5kA9yAsiZ9g98IuvI-1y0WJScqCBdWqlbO5UFew6HC-RAQtlhQjy0STGnXMD6-GjTO9gK0aPSaYG4vR4ROdrf415ZPmWXU44ZoPd2_GFdpjzoQNCfcKjRlHLfbvKMW-hva0DIRZhgncQck972xPX-B4FrOPfbCqTwMWQl9JtkLSdSGFYpaPzVZe4PzKVykVKnjd7Aq-ro8r3tnwNGU8yrcbCdcPdQ3jK6xkzuauY9DDplL3ROFCvNgEEG8BpOCgyU0SqVQXGNrZdpYOBQSlnFGCD1IPv-AKYZ78ASI3PJKUTpBvuJFlFfi85kgwJ19rLGghN4DLU22-fuqMq6cIhN_Hn34UfW5ahvAo_DuwCNe0SXX5i1cgI-UWpGbVr4EToPTYkgsegBxhGNAlaBL9gUXcr4J125YDfuEb5ZVbTMLeVyWrKrRA_Bjz6jORaws1zSJXBac65GM28n0cmV2uycvzrb5ARuvhucWdMR0RxC6SK57uqRJ-I9Ih0ZFG308097nIN1mX5AJWFSY0D67CkUPiYDAjnbkOhg4LEY1jQr_b1L3bBI05hqyq5Hv_fqh7cLBr3Z81N0lr6jiaSX5ugPZZ7C21T5H4i9WcUyFiigKc2X9t_OgCGifv1hu8yBv4mwtPq8v8Na27PSAKHsJNtdWk_KUso7YZBFXV7eZ4w3av3pKDIfo_1-GTAW_BNuari_icePkaX7I3M88oTH6TevEPARqecoM96yEUw3BhvGX-5qYUkKiiFjg83d6n9JV-5VQ05FtZ0SDJB_ldTNv3l7f0dYHD2EwztQ5pSktfG1e0OkdQoMYsiGYqTjYDCWbd4RVQOu6cP5is-EtkHZB0HIcIB7sXkcMTaHB2qmTkZ172orfkoRqMuGSaxiJ_S75zdP4wb8R4n1KpUpSVLGyPHatTKETcAm3-4lHQZQMjN3bZJ7MavgxjM7b-RuN2VRR-zvQLdOuIrpfdKaiUkQ-gm03rt1_xRBZf61OMjI1BDaFbSGEY7tkuk1pv0eqzGIOc6tv5mU2XEWtoKaVf-KVtIY4nD_X7_mgMkHg_Pmmoy7qnZK1GmBlSRIGoItbeGQK9K91IJTgMAiDlK0HmEHxYO6kpy0nzjwsIMA2r1annDnFu6Hojl2NGWWW9_HoXOfUdbRJSl4Y7EdbWmAqRzt0ConZO5aBGpOw8As8QKvy3NZF1oYhAE4-NwzFY0mo3hv2Y6EIIMJX2lrgVn9cwFgTQBz6YfHUL4ZOmQBGXLZkN5KP0NvVrXYYtbVwKrOLCalTWbXXDiYciXe8d6UHzmtEL0eLaTOKMXNOSCTySBb6hSaKYGJazy-cNYVru_lrSL8Ri7fLVLHj7Pe-x8-w-P8dMBlBTfcHqOYntC2NgHLC3lySe9Dl43OyFehtMQ8L7fqXl2x9q0FU6rg9VhXBDhMs4B0n_njW04Num5v1heaCXSFW_PK8gIEDVfciyd_V4q37l_FSwaj5oqdKCVsAHOXHQQuEsMPppZnkBZTVNPt6kvkm0V_qrMijTSVzMZEbV7gY6-g64cZ6chzgIwECqpkq9JU6iJjoejKddInBKn51apdwwBGweV-qDAX0_02_9jcVQpeadTkVAk_6uDQI1n3KPsdFgfipb53fbFirsg96GzD1dFRwLqzGX349-rST7icWZMKPwjlF8XxpoDioJv_sH6TuFTUkoJTulxe2eYdl2Bsp9HP3qh2eUX5JCXeOHxlMIOkY_dVYiJtkEQA2RXRrua0K6EsIA0IxBt3g-c9fMzR-6cVka2QcSQGu2QjUGJV_o9TbdpQTeRXb_-dKSrho4eVIvtfJ1Ob5oPo1ekrQNvE5XdQN0CwyvTUlyMWGuULPa_vk-r45dwQS00uLXFYpU8UyUUIxC0xinQW-Ds3kk08SwKgqleTuWQWgkOX_bBuLeTUPk6X70Qn46aSzt5XY4ljZLovzaCJTpOvR13SLnUetucylm2DQOEP8g3HXu50PaSDHZaJPcrn1aBXTxiUhA_pFfqQi3niCWJvWyIwbKoJS0W1DCk3XT7kbV1SXVy6dLteo8FOjyX-E13RKICUnIgSJDkMbAxrnyYBKZhq5wIi8o8gcU__nnX_9w27scs1a9f6-DCjf-83wZS4YbjeG3s3aNLhfqWPsWAlmGUPPm0ufJeACA-GTlJWh2xlpG6VpQ-auCKj2W7tYZyEb7PUAWcC0TWfUBVuQSyr0-fQfoQoG-hGOarNLzYnBcDL-sN25qKCa-grWRZ4n8HXL-nP1NlxrVpVv4vHQpn1tvkQwQYPhOFvqn1fWOEI6d_PzXqRuHpnF9ZTPYhNlOCs5LCVMXs59ZOyOSERoPswkdcVYvaDrzd-fMykPFXeko1BsNTVBMgUjepHfZNT5V0TsBZkhl_VQIQmuKR1yF-JkZG8nenreudTXjuWt8gIaYyDeFznVOrlbR8AycJXilbvZIGXeCdhGOJTH_-S_md2mrTQn_KPOBihY-JPL4CT-_aCr9xuDu_6a8rpmy49yqv1j_rThq3DHTaVbAYqTh0C-d2WRxEK9ST_LRfZTsfcuvZcncjwoNwzLRLglFjOlKLjVM-eGYLE5xcCcplJ8ot00bcV-uwnPWOWvJXw3jqS4rOUUWAdsF2NQ60FQgNZbHV4m77k5KbE_0oC4js2eFdSfJ5IZwx1n1Fdutq6ylo1KfJNS11Da5rfYp43pM112lciT1B007FYbK7To6K3XSBuqCoxHuCPI5225H-ImqKFgiVUPjbf4lwPQl8LpyGrgsivg64y23-WV_0LBvm-tHwwHZW21eNnMzu0BOwtRrJkiXupKmxeJns0joyWXnGRqHR3ByzuctG4T-ausqau8Z3QFT_h_z__4fQdwhhi4LAAA=';

  
    Drupal.behaviors.powerbi_embeddedJS = {
      attach: function (context, settings) {
        $(window, context).once('powerbi_embeddedJS').each(function () {  
            // add code here to avoid being triggered every AJAX request
            console.log('powerbi_embeddedJS loaded!');

            // Get models. models contains enums that can be used.
            var models = window['powerbi-client'].models;
            var permissions = models.Permissions.All;
            var reportId = 'f6bfd646-b718-44dc-a378-b73e6b528204';
            var groupId = 'be8908da-da25-452e-b220-163f52476cdd';
            //var txtAccessToken = Drupal.theme('generateEmbedToken', reportId, groupId);

            console.log(txtAccessToken);

            var embedConfiguration = {
                type: 'report',
                tokenType: models.TokenType.Embed,
                accessToken: txtAccessToken,
                embedUrl: "https://app.powerbi.com/reportEmbed?reportId=f6bfd646-b718-44dc-a378-b73e6b528204&groupId=be8908da-da25-452e-b220-163f52476cdd&config=eyJjbHVzdGVyVXJsIjoiaHR0cHM6Ly9XQUJJLVVTLU5PUlRILUNFTlRSQUwtcmVkaXJlY3QuYW5hbHlzaXMud2luZG93cy5uZXQifQ%3d%3d",
                id: "f6bfd646-b718-44dc-a378-b73e6b528204",
                permissions: permissions,
                settings: {
                    filterPaneEnabled: true,
                    navContentPaneEnabled: true
                }
            };

            var $reportContainer = $('#reportContainer');
            var report = powerbi.embed($reportContainer.get(0), embedConfiguration);

            // console.log(models.TokenType.Embed);


        });
      },
    };
  })(jQuery, Drupal, drupalSettings);