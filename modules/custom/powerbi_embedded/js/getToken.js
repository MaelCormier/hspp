(function (window, document, $, undefined) {

    "use strict";
  
    $.extend(Drupal.theme, {
      /**
       *
       * @param {string} reportId
       * @param {string} groupId
       * @return {string}
       * 
       * Drupal.theme('generateEmbedToken', 'reportId', 'groupId');
       */
      generateEmbedToken: function(reportId, groupId) {
        let token = reportId + groupId;  
        return token;
      }
    });
  }(window, document, jQuery));