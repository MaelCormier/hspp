/**
 *
 */
;(function ($, Drupal) {

  "use strict";

  Drupal.behaviors.customNav = {
    attach: function (context) {
      $('.navigation-secondary').find('a.is-active').once('customNav').each(function () {
        // Add class to li parent tag
        $(this).parent().addClass('active');
      });
      $('.navigation-main').find('a.is-active').once('customNav').each(function () {
        // Add class to li parent tag
        $(this).parent().addClass('active');
      });
    }
  };

})(jQuery, Drupal);
