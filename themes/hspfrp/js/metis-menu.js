/**
 *
 */
;(function ($, Drupal) {

  "use strict";

  Drupal.behaviors.metisMenuOverride = {
    attach: function (context) {
      $('#menu-sidebar').metisMenu({

        // enabled/disable the auto collapse.
        toggle: true,

        // prevent default event
        preventDefault: true,

        // default classes
        activeClass: 'active',
        collapseClass: 'collapse',
        collapseInClass: 'in',
        collapsingClass: 'collapsing',

        // .nav-link for Bootstrap 4
        triggerElement: 'a',

        // .nav-item for Bootstrap 4
        parentTrigger: 'li',

        // .nav.flex-column for Bootstrap 4
        subMenu: 'ul'

      });

      // Alter href for ul parent element
      $('#menu-sidebar').find(' > li > a').once('metisMenuOverride').each(function () {
        if ($(this).parent().find('ul').length) {
          // Add class to li parent tag
          $(this).attr("href", "#");
        }
      });
    }
  };

})(jQuery, Drupal);
