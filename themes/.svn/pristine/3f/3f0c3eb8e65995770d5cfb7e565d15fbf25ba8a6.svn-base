.bc-gov-alertbanner-success {
  width: 800px;
  margin-bottom: 36px;
}

.bc-gov-alertbanner-error {
  width: 800px;
  margin-bottom: 36px;
}

.bc-gov-alertbanner-important {
  width: 800px;
  margin-bottom: 36px;
}

.bc-gov-alertbanner-warning {
  width: 800px;
  margin-bottom: 36px;
}

.bc-gov-alertbanner-contents {
  display: flex;
  flex-direction: row;
  padding-top: 25px;
  padding-bottom: 30px;
  padding-right: 30px;
}

.bcgov-callout {
  padding: 25px;
  margin: 16px 0;
}

.bcgov-callout h1, h2, h3, h4 {
  margin: 0;
}

@charset "UTF-8";
/* Customize the label (the container) */
.checkbox {
  display: block;
  position: relative;
  padding-left: 25px;
  margin-bottom: 12px;
  cursor: pointer;
  font-family: ‘Noto Sans’, Verdana, Arial, sans-serif;
  font-size: 16px;
  -webkit-user-select: none;
  -moz-user-select: none;
  -ms-user-select: none;
  user-select: none;
}

/* Hide the browser's default checkbox */
.checkbox input {
  position: absolute;
  opacity: 0;
  cursor: pointer;
  height: 0;
  width: 0;
}

/* Create a custom checkbox */
.checkmark {
  position: absolute;
  top: 0;
  left: 0;
  height: 16px;
  width: 16px;
  outline: 2px solid #606060;
}

/* When the checkbox is checked, add a blue background */
.checkbox input:checked ~ .checkmark {
  background-color: #606060;
}

/* Create the checkmark/indicator (hidden when not checked) */
.checkmark:after {
  content: "\2713";
  color: white;
  position: absolute;
  left: 50%;
  top: 50%;
  transform: translate(-50%, -50%);
  display: none;
}

/* Show the checkmark when checked */
.checkbox input:checked ~ .checkmark:after {
  display: block;
}

.date_input_day, .date_input_month {
  height: 25px;
  border: 2px solid #606060;
  margin-top: 5px;
  margin-bottom: 15px;
  border-radius: 4px;
  padding: 5px;
  font-size: 16px;
  width: 32px;
}

.date_input_year {
  height: 25px;
  border: 2px solid #606060;
  margin-top: 5px;
  margin-bottom: 15px;
  border-radius: 4px;
  padding: 5px;
  font-size: 16px;
  width: 64px;
}

.date_input_day[type="text"]:focus {
  outline: 4px solid #38598a;
  outline-offset: 1px;
}

.date_input_month[type="text"]:focus {
  outline: 4px solid #38598a;
  outline-offset: 1px;
}

.date_input_year[type="text"]:focus {
  outline: 4px solid #38598a;
  outline-offset: 1px;
}

.text_label {
  display: flex;
}

.date_field {
  margin-right: 20px;
}

.BC-Gov-PrimaryButton-disabled {
  background-color: #003366;
  opacity: 0.3;
  border: none;
  border-radius: 4px;
  color: white;
  padding: 12px 32px;
  text-align: center;
  text-decoration: none;
  display: block;
  font-size: 18px;
  font-weight: 700;
  letter-spacing: 1px;
  cursor: not-allowed;
}

.BC-Gov-SecondaryButton-disabled {
  background-color: white;
  opacity: 0.3;
  border: 2px solid #003366;
  border-radius: 4px;
  color: #003366;
  padding: 10px 30px;
  text-align: center;
  text-decoration: none;
  display: block;
  font-size: 18px;
  font-weight: 700;
  letter-spacing: 1px;
  cursor: not-allowed;
}

.bc-gov-form {
  display: flex;
  flex-direction: column;
  align-items: flex-start;
}

.bc-gov-dropdown-label {
  margin-bottom: 10px;
}

.bc-gov-dropdown {
  font-size: 18px;
  color: #494949;
  background: white;
  box-shadow: none;
  border: 2px solid #606060;
  min-width: 200px;
  padding: 8px 45px 8px 15px;
  -webkit-appearance: none;
  -moz-appearance: none;
  appearance: none;
}

.fa-chevron-down {
  pointer-events: none;
  position: absolute;
  top: calc(1em - 4px);
  right: 1em;
}

.bc-gov-dropdown-wrapper {
  position: relative;
  display: inline;
}

footer {
  height: 65px;
}

.js-form-type-textfield label,
.js-form-type-textarea label,
.js-form-type-email label {
  display: block;
}

.filter-wrapper {
  display: none;
}

#footer .container,
#footer .container-fluid {
  display: flex;
  justify-content: center;
  flex-direction: column;
  text-align: center;
  height: 46px;
}

#footer ul {
  display: flex;
  flex-direction: row;
  flex-wrap: wrap;
  margin: 0;
  list-style: none;
  align-items: center;
  height: 100%;
}

#footer ul li a {
  font-size: 1.3rem;
  font-weight: normal;
  /* 400 */
  padding-left: 5px;
  padding-right: 5px;
  text-decoration: none;
}

#footer ul li a:hover {
  text-decoration: underline;
}

#footer ul li a:focus {
  outline-offset: 1px;
}

header {
  min-height: 65px;
  width: 100%;
  padding-top: 10px;
  padding-bottom: 10px;
}

header .sitebranding {
  display: flex;
  align-items: center;
}

header h1 {
  font-family: 'Noto Sans', Verdana, Arial, sans-serif;
  font-weight: normal;
  /* 400 */
  visibility: hidden;
}

header .nav-btn {
  display: block;
  width: auto;
  margin: 0 0 0 auto;
  cursor: pointer;
}

header .banner {
  display: flex;
  justify-content: flex-start;
  align-items: center;
  margin: 0 10px 0 0;
}

header .Beta-PhaseBanner {
  color: #fcba19;
  margin-top: -1em;
  text-transform: uppercase;
  font-weight: 600;
  font-size: 16px;
}

header .other {
  display: flex;
  align-items: center;
}

header #block-signout {
  display: flex;
  align-items: center;
  margin-right: 2%;
}

header #block-signout p {
  margin-bottom: 0;
}

/*
    These are sample media queries only. Media queries are quite subjective
    but, in general, should be made for the three different classes of screen
    size: phone, tablet, full.
*/
@media screen and (min-width: 600px) and (max-width: 899px) {
  header h1 {
    font-size: calc(7px + 2.2vw);
    visibility: visible;
  }
}

@media screen and (min-width: 900px) {
  header h1 {
    font-size: 2.0em;
    visibility: visible;
  }
}

a:hover {
  text-decoration: none;
}

/* MAIN NAVIGATION */
.navigation-main {
  display: none;
  width: 100%;
}

.navigation-main .container, .navigation-main .container-fluid {
  padding: 10px 0 10px 0;
}

.navigation-main .container ul, .navigation-main .container-fluid ul {
  display: flex;
  flex-direction: column;
  margin: 0;
  list-style: none;
}

.navigation-main .container ul .active, .navigation-main .container-fluid ul .active {
  font-weight: bold;
}

.navigation-main .container ul li, .navigation-main .container-fluid ul li {
  margin: 5px 0;
}

.navigation-main .container ul li a, .navigation-main .container-fluid ul li a {
  display: flex;
  font-weight: normal;
  /* 400 */
  padding: 0 15px 0 15px;
}

/* SECONDARY NAVIGATION */
.navigation-secondary {
  display: none;
  width: 100%;
}

.navigation-secondary .container, .navigation-secondary .container-fluid {
  padding: 10px 0 10px 0;
}

.navigation-secondary .container ul, .navigation-secondary .container-fluid ul {
  display: flex;
  flex-direction: column;
  margin: 0;
  list-style: none;
}

.navigation-secondary .container ul .active, .navigation-secondary .container-fluid ul .active {
  font-weight: bold;
}

.navigation-secondary .container ul li, .navigation-secondary .container-fluid ul li {
  margin: 5px 0;
}

.navigation-secondary .container ul li a, .navigation-secondary .container-fluid ul li a {
  display: flex;
  font-weight: normal;
  /* 400 */
  padding: 0 15px 0 15px;
  text-decoration: none;
}

/*
  These are sample media queries only. Media queries are quite subjective
  but, in general, should be made for the three different classes of screen
  size: phone, tablet, full.
*/
@media screen and (min-width: 768px) {
  /* MAIN NAVIGATION */
  .navigation-main {
    display: block;
  }
  .navigation-main .container ul,
  .navigation-main .container-fluid ul {
    flex-direction: row;
  }
  .navigation-main .container ul li,
  .navigation-main .container-fluid ul li {
    margin: 0;
  }
  header .nav-btn {
    display: none;
    width: auto;
    margin: 0 0 0 auto;
    cursor: pointer;
  }
  /* SECONDARY NAVIGATION */
  .navigation-secondary {
    display: block;
  }
  .navigation-secondary .container ul,
  .navigation-secondary .container-fluid ul {
    flex-direction: row;
  }
  .navigation-secondary .container ul li,
  .navigation-secondary .container-fluid ul li {
    margin: 0;
  }
  header .nav-btn {
    display: none;
    width: auto;
    margin: 0 0 0 auto;
    cursor: pointer;
  }
}

.BC-Gov-PrimaryButton {
  background-color: #003366;
  color: white;
  border: none;
  border-radius: 4px;
  padding: 12px 32px;
  text-align: center;
  text-decoration: none;
  display: inline-block;
  font-size: 18px;
  font-weight: 700;
  letter-spacing: 1px;
  cursor: pointer;
}

.BC-Gov-PrimaryButton:hover {
  text-decoration: underline;
  opacity: 0.80;
}

.BC-Gov-PrimaryButton:active {
  opacity: 1;
}

.BC-Gov-PrimaryButton-Dark {
  background-color: #fff;
  color: #494949;
  border: none;
  border-radius: 4px;
  padding: 12px 32px;
  text-align: center;
  text-decoration: none;
  display: inline-block;
  font-size: 18px;
  font-weight: 700;
  letter-spacing: 1px;
  cursor: pointer;
}

.BC-Gov-PrimaryButton-Dark:hover {
  text-decoration: underline;
  background-color: #f2f2f2;
}

.BC-Gov-PrimaryButton-Dark:active {
  background-color: #fff;
}

/* Customize the label (the container) */
.radio {
  display: block;
  position: relative;
  padding-left: 30px;
  margin-bottom: 12px;
  cursor: pointer;
  font-size: 18px;
  -webkit-user-select: none;
  -moz-user-select: none;
  -ms-user-select: none;
  user-select: none;
  /* Hide the browser's default radio button */
  /* Create a custom radio button */
}

.radio input {
  position: absolute;
  opacity: 0;
  cursor: pointer;
  height: 0;
  width: 0;
  /* When the radio button is checked, add a blue background */
  /* Custom checkbox has blue outline when in focus */
}

.radio input:checked ~ .dot {
  background-color: #ffffff;
}

.radio input:focus ~ .dot {
  outline: 4px solid #3B99FC;
  outline-offset: 1px;
}

.radio input:checked ~ .dot:after {
  display: block;
}

.radio .dot {
  position: absolute;
  top: 1px;
  left: 0;
  height: 18px;
  width: 18px;
  border-radius: 50%;
  border: 2px solid #606060;
  /* Create the indicator (the dot/circle - hidden when not checked) */
}

.radio .dot:after {
  content: "";
  position: absolute;
  display: none;
  top: 50%;
  left: 50%;
  width: 12px;
  height: 12px;
  border-radius: 50%;
  background: #606060;
  transform: translate(-50%, -50%);
}

#block-searchform {
  display: flex;
  align-items: center;
  margin-left: auto;
  margin-right: 2rem;
}

#block-searchform #search-block-form {
  display: flex;
  justify-content: flex-end;
}

#block-searchform #search-block-form .js-form-type-search:after {
  position: absolute;
  transform: translate(0, -50%);
  right: 10px;
  top: 50%;
  content: "\f002";
  font-family: 'Font Awesome\ 5 Free';
  font-weight: 900;
  font-size: 1.5rem;
}

#block-searchform #search-block-form .BC-Gov-PrimaryButton {
  background-color: #007bff;
  display: none;
}

#block-searchform #search-block-form .text_input {
  margin-top: 0;
  margin-bottom: 0;
  height: 3.5rem;
  width: 30rem;
  border: 0;
}

.BC-Gov-SecondaryButton {
  background: none;
  border-radius: 4px;
  padding: 6px 15px;
  text-align: center;
  text-decoration: none;
  display: block;
  font-size: 1.3rem;
  font-weight: 700;
  letter-spacing: 1px;
  cursor: pointer;
}

.BC-Gov-SecondaryButton:hover {
  opacity: 0.80;
  text-decoration: underline;
  background-color: #003366;
  color: #FFFFFF;
}

.BC-Gov-SecondaryButton:active {
  opacity: 1;
}

.BC-Gov-SecondaryButton-Dark {
  background: none;
  border-radius: 4px;
  border: 2px solid #fff;
  padding: 10px 30px;
  text-align: center;
  text-decoration: none;
  display: block;
  font-size: 18px;
  font-weight: 700;
  letter-spacing: 1px;
  cursor: pointer;
  color: #fff;
}

.BC-Gov-SecondaryButton-Dark:hover {
  text-decoration: underline;
  background-color: #fff;
  color: #494949;
}

.BC-Gov-SecondaryButton-Dark:active {
  background-color: #f2f2f2;
  color: #494949;
}

/* SIDEBAR - LEFT */
aside.layout-sidebar-first {
  margin-top: 15px;
}

aside.layout-sidebar-first div {
  padding: 0px;
}

aside.layout-sidebar-first div .contextual {
  display: none;
}

aside.layout-sidebar-first div ul {
  padding-left: 5px;
  list-style: none;
}

aside.layout-sidebar-first div ul a {
  display: block;
  padding: 10px 12px;
  margin-top: 1px;
}

aside.layout-sidebar-first div ul li a.is-active {
  font-weight: bold;
}

aside.layout-sidebar-first div ul li ul li:first-child a {
  padding-bottom: 0;
}

aside.layout-sidebar-first div ul li ul li:only-child a {
  padding-bottom: 10px;
}

/* SIDEBAR - RIGHT */
aside.layout-sidebar-second {
  margin-top: 15px;
}

.bc-gov-alertbanner-success {
  background-color: #EAF3EC;
  border-left: 10px solid #2E8540;
}

.bc-gov-alertbanner-error {
  background-color: #FBEAEA;
  border-left: 10px solid #D8292F;
}

.bc-gov-alertbanner-important {
  background-color: #EEF4FA;
  border-left: 10px solid #5091CD;
}

.bc-gov-alertbanner-warning {
  background-color: #FEF8ED;
  border-left: 10px solid #F3BD48;
}

.bc-gov-alertbanner-contents {
  color: #494949;
}

.bcgov-callout {
  border-left: 10px solid #38598A;
  background-color: #F2F2F2;
}

#footer {
  background-color: #003366;
  border-top: 1.5px solid #FCBA19;
  color: #ffffff;
}

#footer ul {
  color: #ffffff;
}

#footer ul li a {
  color: #ffffff;
  border-right: 1px solid #4b5e7e;
}

#footer ul li a:hover {
  color: #ffffff;
}

#footer ul li a:focus {
  outline: 4px solid blue;
}

header {
  background-color: #003366;
  border-bottom: 1.5px solid #FCBA19;
  color: #ffffff;
}

a {
  color: #1A5A96;
}

a:hover {
  color: blue;
}

i {
  color: #1A5A96;
}

.navigation-main {
  color: #FCBA19;
  background-color: #38598A;
}

.navigation-main .container ul {
  color: #ffffff;
}

.navigation-main .container ul li a {
  color: #ffffff;
}

.navigation-secondary {
  color: #FCBA19;
  background-color: #38598A;
}

.navigation-secondary .container ul {
  color: #ffffff;
}

.navigation-secondary .container ul li a {
  color: #ffffff;
}

aside.layout-sidebar-first div {
  background-color: #F7F7F7;
}

aside.layout-sidebar-first div ul li a {
  color: #ffffff;
}

aside.layout-sidebar-first div ul li ul {
  background-color: #F7F7F7;
}

aside.layout-sidebar-first div ul li ul li a {
  color: #b3b1b3;
}

aside.layout-sidebar-first a {
  color: #000000;
}

#block-searchform .js-form-type-search:after {
  color: #b3b1b3;
}

.BC-Gov-SecondaryButton {
  border: 2px solid #ffffff;
  color: #ffffff;
}

table, th, td {
  border: 1px solid #000000;
}

th {
  background-color: #EEF4FA;
}

body {
  color: #494949;
}

table {
  border: 1px solid black;
  border-collapse: collapse;
}

table th, table td {
  border: 1px solid black;
  border-collapse: collapse;
  padding: 15px;
}

table th {
  text-align: left;
  background-color: #d0d0d1;
}

table caption {
  padding: 15px;
}

textarea.text_input {
  font-size: 18px;
  font-family: 'Noto Sans', Verdana, Arial, sans-serif;
  border: 2px solid #606060;
  height: auto;
  margin-top: 5px;
  margin-bottom: 15px;
  border-radius: 4px;
  padding: 5px;
  resize: none;
}

textarea.text_input:focus {
  outline: 4px solid #3B99FC;
  outline-offset: 1px;
}

textarea .text_label {
  display: flex;
}

.text_input {
  height: 34px;
  border: 2px solid #606060;
  margin-top: 5px;
  margin-bottom: 15px;
  border-radius: 4px;
  padding: 5px 5px 5px 7px;
}

.text_input[type="text"]:focus {
  outline: 4px solid #3B99FC;
  outline-offset: 1px;
}

.text_label {
  display: flex;
}

/* BC Sans Regular 400 */
@font-face {
  font-family: 'BC Sans';
  src: url("../styles/typography/fonts/BCSans-Regular.eot") format("eot");
  src: url("../styles/typography/fonts/BCSans-Regular.eot?#iefix") format("embedded-opentype"), url("../styles/typography/fonts/BCSans-Regular.woff2") format("woff2"), url("../styles/typography/fonts/BCSans-Regular.woff") format("woff"), url("../styles/typography/fonts/BCSans-Regular.ttf") format("truetype");
  font-weight: 400;
  font-style: normal;
}

/* BC Sans Italic 400 */
@font-face {
  font-family: 'BC Sans Italic';
  src: url("../styles/typography/fonts/BCSans-Italic.eot") format("eot");
  src: url("../styles/typography/fonts/BCSans-Italic.eot?#iefix") format("embedded-opentype"), url("../styles/typography/fonts/BCSans-Italic.woff2") format("woff2"), url("../styles/typography/fonts/BCSans-Italic.woff") format("woff"), url("../styles/typography/fonts/BCSans-Italic.ttf") format("truetype");
  font-weight: 400;
  font-style: italic;
}

/* BC Sans Bold 700 */
@font-face {
  font-family: 'BC Sans Bold';
  src: url("../styles/typography/fonts/BCSans-Bold.eot") format("eot");
  src: url("../styles/typography/fonts/BCSans-Bold.eot?#iefix") format("embedded-opentype"), url("../styles/typography/fonts/BCSans-Bold.woff2") format("woff2"), url("../styles/typography/fonts/BCSans-Bold.woff") format("woff"), url("../styles/typography/fonts/BCSans-Bold.ttf") format("truetype");
  font-weight: 700;
  font-style: normal;
}

/* BC Sans Bold Italic 700 */
@font-face {
  font-family: 'BC Sans Bold Italic';
  src: url("../styles/typography/fonts/BCSans-BoldItalic.eot") format("eot");
  src: url("../styles/typography/fonts/BCSans-BoldItalic.eot?#iefix") format("embedded-opentype"), url("../styles/typography/fonts/BCSans-BoldItalic.woff2") format("woff2"), url("../styles/typography/fonts/BCSans-BoldItalic.woff") format("woff"), url("../styles/typography/fonts/BCSans-BoldItalic.ttf") format("truetype");
  font-weight: 700;
  font-style: italic;
}

html {
  font-size: 62.5%;
}

body {
  font-family: "BC Sans", Verdana, Arial, sans-serif !important;
  color: #003366;
  background-color: #F1F1F1;
  letter-spacing: 0.7px;
}

h1 {
  font-family: "BC Sans Bold", Verdana, Arial, sans-serif;
  font-size: 2.074em;
}

h2 {
  font-family: "BC Sans Bold", Verdana, Arial, sans-serif;
  font-size: 1.728em;
}

h3 {
  font-family: "BC Sans Bold", Verdana, Arial, sans-serif;
  font-size: 1.44em;
}

h4 {
  font-family: "BC Sans Bold", Verdana, Arial, sans-serif;
  font-size: 1.2em;
}

p {
  margin-bottom: 2em;
  font-size: 1.2em;
  letter-spacing: 0.2px;
}

em {
  font-family: "BC Sans Italic", Verdana, Arial, sans-serif;
}

strong {
  font-family: "BC Sans Bold", Verdana, Arial, sans-serif;
}

.small {
  font-size: 1.3rem;
  line-height: 2rem;
}

/* NAVIGATION */
/* SIDEBAR */
aside a {
  font-size: 1.4rem;
  line-height: 1.3;
}
