/* OVERRIDE PARENT THEME CSS */

//----- DEFINE COLOURS ----------------------------//

// Primary B.C. Brand
$header-blue: #003366; // Used for header, footer, primary button
$bc-gold: #FCBA19; // Used for header, footer, beta status
$text: #494949; // Used for headings and paragraphs
$link-blue: #1A5A96; // Used for Links

// Backgrounds
$background-blue: #38598A; // Used for navigation bar
$background-white: #F2F2F2; // Used for backgrounds

// Components
$component-grey: #606060; // Used for text input, textarea, checkbox, radio button

// Semantic Colours
$error: #D8292F; // Used for error messages and indicators
$success: #2E8540; // Used for success messages and indicators

// General
$black: #000000;
$blue: blue;
$light-blue: #5091CD;
$white: #ffffff;
$greenish-white: #EAF3EC;
$pinkish-white: #b3b1b3;
$bluish-white: #EEF4FA;
$orange-yellow: #F3BD48;
$orangish-white: #FEF8ED;
$reddish-white: #FBEAEA;
$grey-blue: #496DA2;
$dark-grey: #666666;

/* BOOTSTRAP OVERRIDE */
main.container-fluid {
  padding-top: 0;
  padding-bottom: 0;
}

// Sidebar
aside.layout-sidebar-first {
  padding-left: 15px;
  padding-right: 0;
}

// Layout content
.layout-content {
  .container-fluid {
    padding-left: 0;
  }
}


// Menu Sidebar - MetisMenus
#menu-sidebar {
  padding-left: 0;
  &>li{
    background-color: $pinkish-white;
  }
}

/* MAIN */
main {
  padding: 15px;
}

.banner h1 {
  margin-bottom: 0;
}

.tab-content {
  h5 {
    font-style: italic;
    margin-bottom: 1rem;
  }
}

i {
  color: #000;
}

/* LAYOUT */
.layout-content .content {
  background-color: #fff;
  padding: 15px;
}

/* NAVIGATION */

/* NAVIGATION MAIN */

.navigation-main {
  .container-fluid {
    padding: 0;

    li {
      padding: 8px 0 8px 0;
    }

    li:hover, li:focus {
      background-color: $grey-blue;
      outline: none;
    }

    li.active a, li a:active, li a:hover, li a:visited {
      text-decoration: none;
      outline: none;
    }

    li.active {
      background-color: $grey-blue;
    }
  }
}

.navigation-main .container ul li a,
.navigation-main .container-fluid ul li a,
.btn a {
  color: #fff;
}

/* NAVIGATION SECONDARY */
.navigation-secondary {
  background-color: $white;
  border-bottom: 3px solid $header-blue;
  margin-bottom: 15px;

  .container {
    margin: 0;
    padding-top: 0;
    padding-bottom: 0;

    ul {
      padding-left: 0;

      li {
        padding-top: 8px;
        padding-bottom: 8px;
        margin-right: 10px;
        background-color: $pinkish-white;
        border-top-left-radius: 8px;
        border-top-right-radius: 8px;
        cursor: pointer;
      }

      li:hover, li:focus {
        background-color: $dark-grey;
        outline: none;
      }

      li.active a, li a:active, li a:hover, li a:visited {
        text-decoration: none;
        outline: none;
      }

      li.active {
        background-color: $header-blue;
      }
    }
  }
}

/* ALERTS BANNERS */
.bc-gov-alertbanner-important {
  width: 100%;
}

.bc-gov-alertbanner-contents {
  padding-left: 30px;
}

/* SERACH */
.form-search.text_input {
  width: 100%;
}

.card-body ol,
.card-body ul {
  list-style: disc;
  margin-left: 2rem;
}

/* LOCAL TASKS */
ul.btn-group {
  padding-left: 0;
}

/* FOOTER */
#footer ul {
  padding-left: 0;
}
