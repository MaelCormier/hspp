.accordion .card-header {
  padding: 0;
}

.accordion .card-header .btn-link {
  display: inline-block;
  font-size: inherit;
  width: 100%;
  text-align: left;
  padding: .75rem 1.25rem;
}

header .other .nav-btn i {
  color: #fff;
}

.ms-webpartPage-root {
  border-spacing: 0;
}

#reportcontainer {
  width: 1280px;
  height: 720px;
}

#framecontainer {
  float: left;
  width: 1244px;
  height: 720px;
}

#shield {
  position: relative;
  float: left;
  width: 34px;
  height: 720px;
  background: white;
  left: -34px;
}

@media screen and (min-width: 899px) and (max-width: 1210px) {
  iframe {
    width: 95% !important;
  }
}

.modal.show .modal-dialog {
  max-width: 80%;
}

.modal .modal-body .btn-group-vertical button {
  margin-bottom: 1rem;
}

/* OVERRIDE PARENT THEME CSS */
/* BOOTSTRAP OVERRIDE */
main.container-fluid {
  padding-top: 0;
  padding-bottom: 0;
}

.bs-region--left {
  padding-left: 15px;
  padding-right: 0;
}

.bs-region--right {
  padding-left: 0;
  padding-right: 15px;
}

.layout-content .container-fluid {
  padding-left: 0;
}

#menu-sidebar {
  padding-left: 0;
}

#menu-sidebar > li {
  background-color: #b3b1b3;
}

/* MAIN */
main {
  padding: 15px;
}

.banner h1 {
  margin-bottom: 0;
}

.tab-content h5 {
  font-style: italic;
  margin-bottom: 1rem;
}

i {
  color: #000;
}

/* LAYOUT */
.layout-content .content {
  background-color: #fff;
  padding: 15px;
}

/* NAVIGATION */
/* NAVIGATION MAIN */
.navigation-main .container-fluid {
  padding: 0;
}

.navigation-main .container-fluid li {
  padding: 8px 0 8px 0;
}

.navigation-main .container-fluid li:hover, .navigation-main .container-fluid li:focus {
  background-color: #496DA2;
  outline: none;
}

.navigation-main .container-fluid li.active a, .navigation-main .container-fluid li a:active, .navigation-main .container-fluid li a:hover, .navigation-main .container-fluid li a:visited {
  text-decoration: none;
  outline: none;
}

.navigation-main .container-fluid li.active {
  background-color: #496DA2;
}

.navigation-main .container ul li a,
.navigation-main .container-fluid ul li a,
.btn a {
  color: #fff;
}

/* NAVIGATION SECONDARY */
.navigation-secondary {
  background-color: #ffffff;
  border-bottom: 3px solid #003366;
  margin-bottom: 15px;
}

.navigation-secondary .container {
  margin: 0;
  padding-top: 0;
  padding-bottom: 0;
}

.navigation-secondary .container ul {
  padding-left: 0;
}

.navigation-secondary .container ul li {
  padding-top: 8px;
  padding-bottom: 8px;
  margin-right: 10px;
  background-color: #b3b1b3;
  border-top-left-radius: 8px;
  border-top-right-radius: 8px;
  cursor: pointer;
}

.navigation-secondary .container ul li:hover, .navigation-secondary .container ul li:focus {
  background-color: #666666;
  outline: none;
}

.navigation-secondary .container ul li.active a, .navigation-secondary .container ul li a:active, .navigation-secondary .container ul li a:hover, .navigation-secondary .container ul li a:visited {
  text-decoration: none;
  outline: none;
}

.navigation-secondary .container ul li.active {
  background-color: #003366;
}

/* ALERTS BANNERS */
.bc-gov-alertbanner-important {
  width: 100%;
}

.bc-gov-alertbanner-contents {
  padding-left: 30px;
}

/* SEARCH */
.form-search.text_input {
  width: 100%;
}

.card-body ol,
.card-body ul {
  list-style: disc;
  margin-left: 2rem;
}

/* LOCAL TASKS */
ul.btn-group {
  padding-left: 0;
}

/* FOOTER */
#footer ul {
  padding-left: 0;
}

#search-block-form {
  display: flex;
  justify-content: flex-end;
}

#search-block-form .BC-Gov-PrimaryButton {
  background-color: #007bff;
}

#search-block-form .text_input {
  margin-top: 0;
  margin-bottom: 0;
  height: 52px;
}

#tiles {
  margin: 15px 0;
  min-height: 50vh;
}

#tiles .list-group-item-action:hover {
  background: none;
}

#tiles h3 {
  background-color: #fff;
  text-align: center;
  cursor: pointer;
  height: 100%;
  padding: 15px;
}

#tiles h3 i {
  font-size: 3.5rem;
  margin-bottom: 0.5rem;
}

#tiles h3:hover {
  z-index: 2;
  color: #fff;
  background-color: #007bff;
}

#tiles h3:hover i {
  color: #fff;
}

#tiles h3:hover.tile-health-outcomes-and-experiences {
  background-color: #38598A;
}

#tiles h3:hover.tile-understand-health-needs {
  background-color: #546fe4;
}

#tiles h3:hover.tile-meet-health-needs {
  background-color: #3a852b;
}

#tiles h3:hover.tile-support-service-providers {
  background-color: #953734;
}

#tiles h3:hover.tile-enable-technology-and-infrastructure {
  background-color: #e3a82b;
}

#tiles h3 span {
  display: block;
}

#tiles .active h3 {
  z-index: 2;
  color: #fff;
  background-color: #007bff;
}

#tiles .active h3 i {
  color: #fff;
}

#tiles .active h3.tile-health-outcomes-and-experiences {
  background-color: #38598A;
}

#tiles .active h3.tile-understand-health-needs {
  background-color: #546fe4;
}

#tiles .active h3.tile-meet-health-needs {
  background-color: #3a852b;
}

#tiles .active h3.tile-support-service-providers {
  background-color: #953734;
}

#tiles .active h3.tile-enable-technology-and-infrastructure {
  background-color: #e3a82b;
}

#tiles .full-title p {
  padding: 0 15px;
  color: #fff;
  font-weight: bold;
}

#tiles .full-title p.outcomes {
  background-color: #38598A;
}

#tiles .full-title p.understand {
  background-color: #546fe4;
}

#tiles .full-title p.meet {
  background-color: #3a852b;
}

#tiles .full-title p.support {
  background-color: #953734;
}

#tiles .full-title p.enable {
  background-color: #e3a82b;
}

/* HEADER */
header h1 {
  font-weight: bold;
}

/* NAVIGATION */
.navigation-main .container-fluid ul li a, .navigation-secondary .container-fluid ul li a {
  font-size: 1.3rem;
  font-weight: bold;
}

.navigation-main .container ul li a, .navigation-secondary .container ul li a {
  font-size: 1.3rem;
  font-weight: bold;
}

@media screen and (max-width: 899px) {
  .banner h1 {
    font-size: 1.25rem;
  }
}

/* BUTTONS */
.btn {
  font-size: 1.6rem;
}

/* FOOTER */
#footer a {
  font-size: 1.2rem;
}
