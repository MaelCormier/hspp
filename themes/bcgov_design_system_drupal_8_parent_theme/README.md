# BCGov Design System Drupal 8 Parent Theme

## Description

A parent theme that can be used for a Drupal 8 project. It will incorporate all of the styles outlined in the British Columbia Government Design System for Digital Services.

The purpose of this theme will be act as a parent to any child theme. The child theme will inherit all the styles. It is based on **stable** and will be referencing its Twig files. With Twig debugging on, it should be clear which templates the child theme will need to override. The parent theme contains some template files to ensure the use of classes specified in the design system.

In the {child-theme}.info.yml, set the base theme to **bcgov_design_system_drupal_8_parent_theme**:

```yml
name: childThemeName
type: theme
description: ''
core: 8.x
base theme: bcgov_design_system_drupal_8_parent_theme
```
It has been developed to be UI framework agnostic. It does not impose any grid, so the child them may choose to use Bootstrap, Bulma, Materialize, etc.

## Using Gulp 4

From the command line, in this theme's folder, run:

```bash
npm install
```

This will install all project dependencies and create a node_modules folder.

> !!! DO NOT COMMIT YOUR node_modules FOLDER !!!

### Gulp Task Commands

I've listed the commands to use if you have Gulp 3 or Gulp 4 installed. On the command line, type:

```bash
gulp -version
```

If the CLI version is < 4, you will have to use the node command to use the Gulp version stored in node_modules. The version of node installed should be >= 8

#### CSS 

Convert files in the scss directory into single css files (styles.css, styles.min.css) and move to css directory. Its possible to create as many scss files as needed and they will be compiled by Gulp into a single file.

```bash
gulp
```

```bash
node ./node_modules/gulp/bin/gulp.js
```

#### Watch

Watch changes in the scss directory and trigger 'css' Gulp function. As soon as an scss file is saved, it will trigger the compile process.

```bash
gulp watch
```

```bash
node ./node_modules/gulp/bin/gulp.js watch
```

## Theme Settings

In theme-settings.php, it has the option to define the status of the application. 

## References

[British Columbia Government Design System for Digital Services](https://github.com/bcgov/design-system)
